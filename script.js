console.log("DOM MANIPULATION");

/*
	This is a WEB PAGE

	<html>
		<head>
			<title></title>
		</head>
		<body>
			<div>
				<h1></h1>
				<p></p>
			</div>
		</body>
	</html>
*/

/*DOM
	
	DOM = {
		document: {
			html: {
				head: {
					title: {
	
					},
					meta: {
	
					}
				},
				body: {
					div: {
						h1: "",
						p: ""
					}
				}
			}
		}
	}

*/

//Finding HTML Elements
	console.log(document.getElementById("demo"));
	console.log(document.getElementsByTagName("h1"));
	console.log(document.getElementsByClassName("title"));

	console.log(document.querySelector("#demo"));
	console.log(document.querySelector("h1"));
	console.log(document.querySelector(".title"));

//Change HTML Elements
    //Property
        //Element.innerHTML = new HTML content
        let myH1 = document.querySelector(".title");
        myH1.innerHTML = "Hello world";

        //element.attribute = new value
        document.querySelector("title").color = "Red"; 

        //remove Attribute
        document.getElementById("demo").removeAttribute("class");

let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");

let fullName = document.getElementById("fullName");

const updateName = () => {
    let txtFirstName = firstName.value;
    let txtLastName = lastName.value;

    fullName.innerHTML = `${txtFirstName} ${txtLastName}`;
};

firstName.addEventListener("keyup", updateName);

lastName.addEventListener("keyup", updateName);